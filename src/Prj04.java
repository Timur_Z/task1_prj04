import java.io.BufferedReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Prj04 {
    public static void main(String[] args) {

//        Scanner scanner = new Scanner(System.in);

        String[] stringArr = {"one","two","three","four","five","six","seven","eight","nine","ten"};

        Arrays.sort(stringArr, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return s.length() - t1.length();
            }
        });

        for (String word : stringArr)
            System.out.println(word);






    }
}
